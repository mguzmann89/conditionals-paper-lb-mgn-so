Data and code for the paper: "Socio-linguistic effects on conditional constructions: A quantitative typological study" By Laura Becker, Matías Guzmán Naranjo and Samira Ochs

**Data***

- ./data/dfsum.rds
  main dataset in rds format

- ./data/dfsum.csv
  main dataset in csv format

- ./data/languoid.csv
  from glottolog the languoid to build the phylogenetic tree

- ./data/df-phylo.rds
  the tree resulting from generate-phylogeny.r

**To run the code:***

- run generate-phylogeny.r
- run model.r
- run analyze.r
- run plots.r
