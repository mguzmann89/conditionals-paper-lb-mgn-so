library(tidyverse)
library(viridis)
library(sf)
library(rnaturalearth)
library(rnaturalearthdata)
library(rgeos)

## import data

df <- read_rds("./data/dfsum.rds")

## Figure 1

world <- ne_countries(scale = "medium", returnclass = "sf")

p <- ggplot(world) +
    geom_sf(color = "#bfbfbf",
            fill = "#d9d9d9",
            size = 0.5) +
    theme(panel.grid.major = element_line(color = "NA"),
          panel.background = element_rect(fill = "#ffffff")) +
    geom_point(data = df, aes(x = longitude,
                              y = latitude,
                              color = N_dedicate),
               size = 2,
               position = "jitter") +
    scale_color_viridis(name = "N dedicated constructions",
                        option = "rocket") +
    theme(legend.position = "bottom") +
    xlab("") +
    ylab("")
p

ggsave(filename = "map.pdf",
       width = 23, height = 14, units = "cm",
       plot = p)


## Figure 2 left plot

p <- ggplot(df, aes(x = log_speaker,
                   y = N_dedicate)) +
    geom_point(color = "#003366") +
    xlab("log N speaker") +
    ylab("N conditional cxt")
p

ggsave(filename = "cxt-nspeaker.pdf",
       width = 12,
       height = 12,
       units = "cm",
       plot = p)

## Figure 2 right plot

p <- ggplot(df) +
    geom_jitter(aes(x = multilinguals,
                    y = N_dedicate,
                    color = multilinguals),
                width = 0.3,
                height = 0.1) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("multilingual speakers") +
    ylab("N conditional cxt")
p

ggsave(filename = "cxt-multilinguals.pdf",
       width = 9,
       height = 12,
       units = "cm",
       plot = p)


## Figure 3 left plot

p <- ggplot(df) +
    geom_jitter(aes(x = edu,
                    y = N_dedicate,
                    color = edu),
                width = 0.3,
                height = 0.1) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("education") +
    ylab("N conditional cxt")
p

ggsave(filename = "cxt-edu.pdf",
       width = 8,
       height = 12,
       units = "cm",
       plot = p)


## Figure 3 center plot

p <- ggplot(df) +
    geom_jitter(aes(x = writing,
                    y = N_dedicate,
                    color = writing),
                width = 0.3,
                height = 0.1) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("use in writing") +
    ylab("N conditional cxt")
p

ggsave(filename = "cxt-writing.pdf",
       width = 7,
       height = 12,
       units = "cm",
       plot = p)

## Figure 3 right plot

p <- ggplot(df) +
    geom_jitter(aes(x = lit,
                    y = N_dedicate,
                    color = lit),
                width = 0.3,
                height = 0.1) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("literature tradition") +
    ylab("N conditional cxt")
p

ggsave(filename = "cxt-lit.pdf",
       width = 6,
       height = 12,
       units = "cm",
       plot = p)

## Figure 4 left plot

p <- ggplot(df, aes(x = edu ,
                   y = log_speaker,
                   color = edu)) +
    geom_jitter(width = 0.3) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("education") +
    ylab("log N speakers")
p

ggsave(filename = "nspeaker-edu.pdf",
       width = 8,
       height = 12,
       units = "cm",
       plot = p)

## Figure 4 center plot

p <- ggplot(df, aes(x = writing ,
                   y = log_speaker,
                   color = writing)) +
    geom_jitter(width = 0.3) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("use in writing") +
    ylab("log N speakers")
p

ggsave(filename = "nspeaker-writing.pdf",
       width = 7,
       height = 12,
       units = "cm",
       plot = p)

## Figure 4 right plot

p <- ggplot(df, aes(x = lit ,
                   y = log_speaker,
                   color = lit)) +
    geom_jitter(width = 0.3) +
    scale_color_viridis(discrete = TRUE, begin = 1, end = 0) +
    theme(legend.position = "none") +
    xlab("literature tradition") +
    ylab("log N speakers")
p

ggsave(filename = "nspeaker-lit.pdf",
       width = 6,
       height = 12,
       units = "cm",
       plot = p)


## Figure 5

p <- ggplot(df, aes(x = grammar,
                   y = N_dedicate)) +
    geom_point() +
    geom_point(color = "#003366") +
    xlab("grammar length in pages") +
    ylab("N conditional cxt") +
    theme(legend.position = "none")
p

ggsave(filename = "cxt-grammarlength.pdf",
       width = 14,
       height = 12,
       units = "cm",
       plot = p)
