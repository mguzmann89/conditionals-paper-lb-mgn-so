library(tidyverse)
library(brms)
library(projpred)
library(ggthemr)
ggthemr("fresh")

## no controls

########################################
## with controls
########################################

bm_gpp <- read_rds("./models-2/bm-gpp.rds")
bm_gpp2 <- read_rds("./models-2/bm-gpp2.rds")
bm_gpp3 <- read_rds("./models-2/bm-gpp3.rds")
bm_gpp4 <- read_rds("./models-2/bm-gpp4.rds")
bm_gpp5 <- read_rds("./models-2/bm-gpp5.rds")
bm_gpp6 <- read_rds("./models-2/bm-gpp6.rds")
bm_gpp7 <- read_rds("./models-2/bm-gpp7.rds")

ce_bm_gpp2 <- conditional_effects(bm_gpp2
                               , effects = c("log_speaker_s", "edu", "writing"
                                           , "lit", "multilinguals2"))

ce_plot_bm_gpp2_1 <- as_tibble(ce_bm_gpp2[[1]]) %>%
    ggplot(aes(x = log_speaker_s, y = estimate__))+
    geom_ribbon(aes(ymin = lower__, ymax = upper__), alpha = 0.5)+
    geom_line(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("log speakers")
ce_plot_bm_gpp2_1

ggsave("./plots-2/ce-bm-gpp2-1.pdf", ce_plot_bm_gpp2_1, height = 4, width = 6)

ce_plot_bm_gpp2_2 <- as_tibble(ce_bm_gpp2[[2]]) %>%
    ggplot(aes(x = edu, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("education")
ce_plot_bm_gpp2_2

ggsave("./plots-2/ce-bm-gpp2-2.pdf", ce_plot_bm_gpp2_2, height = 4, width = 6)

ce_plot_bm_gpp2_3 <- as_tibble(ce_bm_gpp2[[3]]) %>%
    ggplot(aes(x = writing, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("writing")
ce_plot_bm_gpp2_3

ggsave("./plots-2/ce-bm-gpp2-3.pdf", ce_plot_bm_gpp2_3, height = 4, width = 6)

ce_plot_bm_gpp2_4 <- as_tibble(ce_bm_gpp2[[4]]) %>%
    ggplot(aes(x = lit, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("literature")
ce_plot_bm_gpp2_4

ggsave("./plots-2/ce-bm-gpp2-4.pdf", ce_plot_bm_gpp2_4, height = 4, width = 6)

ce_plot_bm_gpp2_5 <- as_tibble(ce_bm_gpp2[[5]]) %>%
    ggplot(aes(x = multilinguals2, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("multilinguals")
ce_plot_bm_gpp2_5

ggsave("./plots-2/ce-bm-gpp2-5.pdf", ce_plot_bm_gpp2_5, height = 4, width = 6)


##
ce_bm_gpp3 <- conditional_effects(bm_gpp3, effects = c("log_speaker_s"))

ce_plot_bm_gpp3_1 <- as_tibble(ce_bm_gpp3[[1]]) %>%
    ggplot(aes(x = log_speaker_s, y = estimate__))+
    geom_ribbon(aes(ymin = lower__, ymax = upper__), alpha = 0.5)+
    geom_line(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("log speakers")
ce_plot_bm_gpp3_1

ggsave("./plots-2/ce-bm-gpp3.pdf", ce_plot_bm_gpp3_1, height = 4, width = 6)

##

bm4 <- read_rds("./models-2/bm-gpp4.rds")

ce_bm4 <- conditional_effects(bm4, effects = "edu")

ce_plot_bm4 <- as_tibble(ce_bm4[[1]]) %>%
    ggplot(aes(x = edu, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("education")
ce_plot_bm4

ggsave("./plots-2/ce-bm-gpp4.pdf", ce_plot_bm4
     , height = 4, width = 6)

bm5 <- read_rds("./models-2/bm-gpp5.rds")

ce_bm5 <- conditional_effects(bm5, effects = "writing")

ce_plot_bm5 <- as_tibble(ce_bm5[[1]]) %>%
    ggplot(aes(x = writing, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("writing")
ce_plot_bm5

ggsave("./plots-2/ce-bm-gpp5.pdf", ce_plot_bm5
     , height = 4, width = 6)

bm6 <- read_rds("./models-2/bm-gpp6.rds")

ce_bm6 <- conditional_effects(bm6, effects = "lit")

ce_plot_bm6 <- as_tibble(ce_bm6[[1]]) %>%
    ggplot(aes(x = lit, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("literature")
ce_plot_bm6

ggsave("./plots-2/ce-bm-gpp6.pdf", ce_plot_bm6
     , height = 4, width = 6)

bm7 <- read_rds("./models-2/bm-gpp7.rds")

ce_bm7 <- conditional_effects(bm7, effects = "multilinguals2")

ce_plot_bm7 <- as_tibble(ce_bm7[[1]]) %>%
    ggplot(aes(x = multilinguals2, y = estimate__))+
    geom_errorbar(aes(ymin = lower__, ymax = upper__))+
    geom_point(color = "#E84646")+
    ylim(c(0, 5))+
    ylab("N. constructions")+
    xlab("multilinguals")
ce_plot_bm7

ggsave("./plots-2/ce-bm-gpp7.pdf", ce_plot_bm7
     , height = 4, width = 6)
