library(tidyverse)
library(brms)
library(ape)
library(projpred)
options(mc.cores = 100)

df_fin <- read_rds("./data/dfsum-2.rds") %>% as_tibble()

df_fin <- mutate(df_fin
     , multilinguals2 = as.character(multilinguals) %>%
           str_replace("few", "some") %>%
           factor(levels = c("unknown", "some", "many"
                       , "most", "all")
                , ordered = TRUE))

df_phylo <- read_rds("./data/df-phylo.rds")
phylo_mat <- ape::vcv(df_phylo, cor = TRUE)

df_fin <- mutate(df_fin
               , grammar_s = scale(grammar)
               , log_speaker_s = scale(log_speaker)) %>%
    rename(lon = longitude
         , lat = latitude)

########################################
########################################

bm_1 <- brm(N_dedicate ~ 1
          , family = poisson
          , data = df_fin
          , prior = c(prior(normal(0, 2)
                          , class = Intercept))
          , seed = 1234, cores = 4
          , chains = 4, iter = 1000, warmup = 500)

write_rds(bm_1, "./models-2/bm1.rds")

bm_p <-
    brm(N_dedicate ~ 1
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.8))

write_rds(bm_p, "./models-2/bm-p.rds")

bm_gp <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

bm_gp

write_rds(bm_gp, "./models-2/bm-gp.rds")

## phylo + gp

bm_gpp <-
    brm(N_dedicate ~ 1
        + (1 | gr(glottocode, cov = phylo))
        + grammar_s
        + gp(lon, lat, by = area)
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp, "./models-2/bm-gpp.rds")

bm_gpp_2 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + log_speaker_s
        + (edu)
        + (writing)
        + (lit)
        + (multilinguals2)
        + grammar_s
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp_2, "./models-2/bm-gpp2.rds")

##

bm_gpp_3 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + grammar_s
        + log_speaker_s
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp_3, "./models-2/bm-gpp3.rds")

##

bm_gpp_4 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + grammar_s
        + edu
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1235, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98
                     , max_treedepth = 12))

write_rds(bm_gpp_4, "./models-2/bm-gpp4.rds")

bm_gpp_5 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + grammar_s
        + writing
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp_5, "./models-2/bm-gpp5.rds")

bm_gpp_6 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + grammar_s
        + lit
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp_6, "./models-2/bm-gpp6.rds")

bm_gpp_7 <-
    brm(N_dedicate ~ 1
        + gp(lon, lat, by = area)
        + grammar_s
        + multilinguals2
        + (1 | gr(glottocode, cov = phylo))
      , family = poisson
      , data = df_fin
      , data2 = list(phylo = phylo_mat)
      , prior = c(prior(normal(0, 2), class = Intercept)
                , prior(normal(0, 1), class = b)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 0.3), class = lscale
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = lscale
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 0.2), class = lscale
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 0.5), class = lscale
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAfrica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaAustralia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaEurasia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaNorthAmerica)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaPapunesia)
                , prior(normal(0, 1), class = sdgp
                      , coef = gplonlatareaSouthAmerica)
                , prior(normal(0, 1), class = sd)
                  )
      , seed = 1234, cores = 4
      , chains = 4, iter = 6000, warmup = 2000
      , control = list(adapt_delta = 0.98))

write_rds(bm_gpp_7, "./models-2/bm-gpp7.rds")

##########

bm_1_k <- kfold(bm_1, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_1_k, "./models-2/bm1-k.rds")

bm_p_k <- kfold(bm_p, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_p_k, "./models-2/bm-p-k.rds")

bm_gp_k <- kfold(bm_gp, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gp_k, "./models-2/bm-gp-k.rds")

bm_gpp_k <- kfold(bm_gpp, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_k, "./models-2/bm-gpp-k.rds")

bm_gpp_2_k <- kfold(bm_gpp_2, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_2_k, "./models-2/bm-gpp2-k.rds")

bm_gpp_3_k <- kfold(bm_gpp_3, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_3_k, "./models-2/bm-gpp3-k.rds")

##

bm_gpp_4_k <- kfold(bm_gpp_4, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_4_k, "./models-2/bm-gpp4-k.rds")

bm_gpp_5_k <- kfold(bm_gpp_5, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_5_k, "./models-2/bm-gpp5-k.rds")

bm_gpp_6_k <- kfold(bm_gpp_6, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_6_k, "./models-2/bm-gpp6-k.rds")

bm_gpp_7_k <- kfold(bm_gpp_7, save_fits = TRUE
              , cores = 4
              , silent = TRUE
              , open_progress=FALSE)

write_rds(bm_gpp_7_k, "./models-2/bm-gpp7-k.rds")

########################################

bm_gpp_k <- read_rds("./models/bm-gpp-k.rds")
bm_gp_k <- read_rds("./models/bm-gp")
bm_p_k <- read_rds("./models/bm-p-k.rds")

bm_gpp_2_k <- read_rds("./models/bm-gpp2-k.rds")
bm_gpp_3_k <- read_rds("./models/bm-gpp3-k.rds")
bm_gpp_4_k <- read_rds("./models/bm-gpp4-k.rds")
bm_gpp_5_k <- read_rds("./models/bm-gpp5-k.rds")
bm_gpp_6_k <- read_rds("./models/bm-gpp6-k.rds")
bm_gpp_7_k <- read_rds("./models/bm-gpp7-k.rds")
bm_1_k <- read_rds("./models/bm1-k.rds")


loo_compare(bm_1_k
          , bm_gpp_k
          , bm_gp_k
          , bm_p_k
          , bm_gpp_2_k
          , bm_gpp_3_k
          , bm_gpp_4_k
          , bm_gpp_5_k
          , bm_gpp_6_k
          , bm_gpp_7_k)
